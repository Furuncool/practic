import Menu from "./components/Menu";
import styled from "styled-components";
import Main from "./components/Main";
import RightColumn from "./components/RightColumn";

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`

function App() {
  return (
    <Wrapper>
      <Menu/> 
      <Main/>
      <RightColumn/>
    </Wrapper>
  );
}

export default App;
