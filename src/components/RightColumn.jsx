import React from "react";
import styled from "styled-components";
import { css } from "styled-components";
import Avatar from "../images/avatar.png";
import RightColumnInput from "./RightColumnInput";

const StyledRightColumn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  gap: 65px;
  width: 465px;
  padding-top: 22px;

`;

const UserBlock = styled.div`
  display: flex;
  gap: 6px;
  margin-right: 44px;
  align-items: flex-end;
`;

const UserNameBlock = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding-bottom: 3px;
`;
const UserName = styled.p`
  font-weight: 600;
  font-size: 20px;
  color: #224973;
`;

const UserMail = styled.p`
  font-size: 16px;
  color: #9498a0;
`;

const ComponentsBlock = styled.div`
  background-color: #e8e8e8;
  padding: 38px 22px 22px;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

const Choose = styled.div`
  display: flex;
  gap: 55px;
  margin-bottom: 40px;
`;

const ChooseItem = styled.button`
  background: none;
  font-size: 16px;
  color: #9498a0;
  padding-bottom: 7px;
  position: relative;

  ::after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 1px;
    background-color: #9498a0;
  }

  ${(props) =>
    props.active &&
    css`
      color: #181818;
      ::after {
        background-color: #224973;
      }
    `}
`;

const SubChoose = styled.p`
  font-size: 16px;
  color: #9498a0;
  margin-bottom: 22px;
`;

const ComponentsList = styled.div`
  display: flex;
  flex-direction: column;
  gap: 22px;
  width: 100%;
`

const ComponentsListItem = styled.div`
  width: 100%;
  background-color: #fff;
  height: 188px;
`

const RightColumn = () => {
  return (
    <StyledRightColumn>
      <UserBlock>
        <img src={Avatar} />
        <UserNameBlock>
          <UserName>Пусилий Котовский</UserName>
          <UserMail>kotik_kompotik@gmail.com</UserMail>
        </UserNameBlock>
      </UserBlock>

      <ComponentsBlock>
        <Choose>
          <ChooseItem>Редактор</ChooseItem>
          <ChooseItem active>Списки</ChooseItem>
        </Choose>

        <SubChoose>
          Выберите список, из которого хотите добавить параграф:
        </SubChoose>

        <RightColumnInput />

        <ComponentsList>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>
          <ComponentsListItem></ComponentsListItem>

        </ComponentsList>
      </ComponentsBlock>
    </StyledRightColumn>
  );
};

export default RightColumn;
