import React, { Children } from 'react';
import styled from 'styled-components';
import { css } from 'styled-components';

const StyledButton = styled.button`
  height: 40px;
  padding: 0 28px;
  border: 1px solid #181818;

  ${(props) => props.secondary && css`
    background-color: #181818;
    color: #fff;
  `}
`

const Button = ({...props}) => {
  return (
    <StyledButton {...props}/>
  );
};

export default Button;