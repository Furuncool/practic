import React from 'react';
import styled from 'styled-components';
import Loupe from "../images/icons/loupe.svg"

const StyledRightColumnInput = styled.div`
  display: flex;
  border-bottom: 1px solid #224973;
  gap: 5px;
  width: 100%;
  padding-bottom: 10px;
  margin-bottom: 40px;
`

const StyledInput = styled.input`
  width: 100%;
  background-color: rgba(0,0,0,0);
  font-size: 16px;

`

const RightColumnInput = () => {
  return (
    <StyledRightColumnInput>
      <img src={Loupe} alt="" />
      <StyledInput/>
    </StyledRightColumnInput>
  );
};

export default RightColumnInput;